import explosionImageURL from './res/explosion_3.png'
import 'pixi.js'
import GameStage from './GameStage';
import Main from './main';

export default class Explosion extends PIXI.Sprite
{
    constructor(x: number, y: number)
    {
        super(PIXI.Texture.fromImage(explosionImageURL))
        this.x = x
        this.y = y

        this.anchor.x = 0.5
        this.anchor.y = 0.5
        this.rotation = Math.random() * 360

        Main.instance.app.ticker.add(() => this.update())
        GameStage.instance.world.addChild(this)
    }

    update()
    {
        this.alpha -= 0.1

        if (this.alpha <= 0) {
            GameStage.instance.world.removeChild(this)
        }
    }
}