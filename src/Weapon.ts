export default abstract class Weapon
{
	damage: number = 0
	speed: number = 0
	fireRate: number = 0
	bulletImageUrl: string = ""
	itemImageUrl: string = ""
	playerSpeedModification: number = 0
	scatter: number = 0
}