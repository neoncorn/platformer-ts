import Stage from "./Stage";
import endLevelImageUrl from './res/endlvl.png'
import Main from "./main";
import GameStage from "./GameStage";

export default class EndLevelStage extends Stage
{
	constructor()
	{
		super()

		var background = new PIXI.Sprite(PIXI.Texture.fromImage(endLevelImageUrl))
		this.addChild(background)

		background.interactive = true
		background.on("mouseup", (event: PIXI.interaction.InteractionEvent) =>
		{
			Main.instance.showStage(new GameStage())
		});
	}
}