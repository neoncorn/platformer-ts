import Character from "./Character";
import GameStage from "./GameStage";
import Main from "./main";
import GameObject from "./GameObject";
import Explosion from "./Explosion";

export default class Bullet extends GameObject
{
	damage: number
	owner: Character

	constructor(owner: Character, damage: number, imageUrl: string, speedX: number, speedY: number)
    {
		super(10)

		this.texture = PIXI.Texture.fromImage(imageUrl)
		
		this.owner = owner
		this.damage = damage
        this.x = owner.x
		this.y = owner.y
		this.speedX = speedX
		this.speedY = speedY

		this.gravity = 0

        GameStage.instance.addGameObject(this)
	}

	didHitGround(angle: number)
	{
		new Explosion(this.x, this.y)

		GameStage.instance.removeGameObject(this)
	}

	didHitGameObject(gameObject: GameObject)
	{
		if (gameObject == this.owner) {
			return
		}
		if (gameObject instanceof Bullet) {
			return
		}

		new Explosion(this.x, this.y)
		GameStage.instance.removeGameObject(this)
	}
}