import playerImageURL from './res/player_00.png'
import Character from "./Character";
import KeyboardManager from './KeyboardManager';
import MouseManager from './MouseManager';
import GameStage from './GameStage';
import PistolWeapon from './GameObjects/PistolWeapon';

export default class Player extends Character
{
	keyboardManager = new KeyboardManager()
	mouseManager = new MouseManager(GameStage.instance)

	constructor()
	{
		super(50, new PistolWeapon())

		var sprite = new PIXI.Sprite(PIXI.Texture.from(playerImageURL))
		sprite.anchor.x = 0.5
		sprite.anchor.y = 0.5
		this.addChild(sprite)
	}

	update()
	{
		if (this.keyboardManager.isDown("ArrowRight")) {
			this.speedX = 3
		}

		if (this.keyboardManager.isDown("ArrowLeft")) {
			this.speedX = -3
		}

		if (this.keyboardManager.isDown("ArrowUp")) {
			this.jump()
		}

		if (this.keyboardManager.isDown("ControlLeft") || this.mouseManager.mouseDown) {
			let cursorWorldX = this.mouseManager.mouseX - GameStage.instance.world.getGlobalPosition().x
			let cursorWorldY = this.mouseManager.mouseY - GameStage.instance.world.getGlobalPosition().y
			this.fire(cursorWorldX, cursorWorldY)
		}

		super.update()
	}
}