import GameObject from "./GameObject";
import mineImageURL from './res/explosion_2.png'
import GameStage from "./GameStage";

export default class Mine extends GameObject
{
	constructor()
	{
		super(30)

		var sprite = new PIXI.Sprite(PIXI.Texture.from(mineImageURL))
		sprite.anchor.x = 0.5
		sprite.anchor.y = 0.5
		this.addChild(sprite)
	}

	didHitGameObject(gameObject: GameObject)
	{
		if (gameObject == GameStage.instance.player) {
			GameStage.instance.player.health -= 50
			GameStage.instance.removeGameObject(this)
		}
	}
}