import GameObject from "./GameObject";
import coinImageUrl from './res/coin.png'
import GameStage from "./GameStage";

export default class Coin extends GameObject
{
	constructor()
	{
		super(30)

		var sprite = new PIXI.Sprite(PIXI.Texture.from(coinImageUrl))
		sprite.anchor.x = 0.5
		sprite.anchor.y = 0.5
		this.addChild(sprite)
	}

	didHitGameObject(gameObject: GameObject)
	{
		if (gameObject == GameStage.instance.player) {
			GameStage.instance.removeGameObject(this)
		}
	}
}