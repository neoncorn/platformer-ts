import * as PIXI from 'pixi.js';
import Stage from './Stage';
import Main from './main';
import Player from './Player';

import level1 from './res/level1.json'
import level2 from './res/level2.json'

import level1Image from './res/level-1.png'
import Level from './Level';
import GameObject from './GameObject';
import Enemy from './Enemy';
import WeaponItem from './GameObjects/WeaponItem';
import ShotgunWeapon from './GameObjects/ShotgunWeapon';
import HealthPack from './HealthPack';
import Coin from './Coin';

export default class GameStage extends Stage
{
	currentLevel!: Level
	currentLevelIndex = 0
	levels = [
		level1,
		level2
	]

	player!: Player
	gameObjects: Array<GameObject> = []

	healthLabel = new PIXI.Text()

	world = new PIXI.Container()
	targetWorldX = 0
	targetWorldY = 0

	static instance: GameStage

	constructor()
	{
		super()

		GameStage.instance = this
		
		this.loadLevel(level1)

		this.healthLabel.style.fill = 0xffffff
		this.healthLabel.text = "Health: "
		this.addChild(this.healthLabel)

		setTimeout(() => {
			Main.instance.app.ticker.add(this.update, this)
		}, 100)
	}

	loadLevel(level: any)
	{
		//remove old level
		this.removeChild(this.world)
		this.gameObjects = []

		//create new level
		this.world = new PIXI.Container()
		this.addChild(this.world)

		this.currentLevel = new Level(level["foreground"])
		this.world.addChild(this.currentLevel)
		
		this.player = new Player()
		this.addGameObject(this.player)

		this.player.x = level["start_position"]["x"]
		this.player.y = level["start_position"]["y"]

		for (var enemyData of level["enemies"]) {
			let enemy = new Enemy()
			enemy.x = enemyData["x"]
			enemy.y = enemyData["y"]
			this.addGameObject(enemy)
		}

		for (var itemData of level["items"]) {
			if (itemData["type"] == "shotgun") {
				var shotgun = new ShotgunWeapon()
				let item = new WeaponItem(shotgun)
				item.x = itemData["x"]
				item.y = itemData["y"]
				this.addGameObject(item)
			}
			if (itemData["type"] == "health_pack") {
				let item = new HealthPack()
				item.x = itemData["x"]
				item.y = itemData["y"]
				this.addGameObject(item)
			}
			if (itemData["type"] == "coin") {
				let item = new Coin()
				item.x = itemData["x"]
				item.y = itemData["y"]
				this.addGameObject(item)
			}
		}
	}

	loadNextLevel()
	{
		this.currentLevelIndex += 1
		this.loadLevel(this.levels[this.currentLevelIndex])
	}

	shouldLoadNextLevel(): boolean
	{
		var enemiesLeft = 0
		for (var gameObject of this.gameObjects) {
			if (gameObject instanceof Enemy) {
				enemiesLeft += 1
			}
		}

		if (enemiesLeft == 0) {
			return true
		} else {
			return false
		}
	}
	
	getPickedCoinCount(): number
	{
		var coinsLeft = 0
		for (var gameObject of this.gameObjects) {
			if (gameObject instanceof Coin) {
				coinsLeft += 1
			}
		}
		return 3 - coinsLeft
	}

	update()
	{
		for (var gameObject of this.gameObjects) {
			this.currentLevel.collide(gameObject)
		}

		for (var gameObject of this.gameObjects) {
			for (var other of this.gameObjects) {
				if (gameObject == other) {
					continue
				}

				var dx = gameObject.x - other.x
				var dy = gameObject.y - other.y
				var distance = Math.sqrt(dx*dx + dy*dy)

				if (distance < gameObject.radius + other.radius) {
					gameObject.didHitGameObject(other)
					other.didHitGameObject(gameObject)
				}

			}
		}

		this.moveCamera()

		this.healthLabel.text = "Health: " + this.player.health
	
		if (this.shouldLoadNextLevel()) {
			this.loadNextLevel()
		}
	}

	addGameObject(gameObject: GameObject)
	{
		this.world.addChild(gameObject)
		this.gameObjects.push(gameObject)
	}

	removeGameObject(gameObject: GameObject)
	{
		this.world.removeChild(gameObject)
		var index = this.gameObjects.indexOf(gameObject)
		if (index == -1) {
			//do nothing
		} else {
			this.gameObjects.splice(index, 1)
		}
	}

	moveCamera()
	{
		this.targetWorldX = -this.player.x + Main.instance.app.renderer.width/2
		this.targetWorldY = -this.player.y + Main.instance.app.renderer.height/2

		this.world.x = this.world.x + (this.targetWorldX - this.world.x)*0.03
		this.world.y = this.world.y + (this.targetWorldY - this.world.y)*0.03
	}
}