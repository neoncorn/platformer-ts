import Main from "./main";

export default class GameObject extends PIXI.Sprite
{
	speedX: number = 0
	speedY: number = 0
	radius: number
	gravity: number = 0.3

	constructor(radius: number)
	{
		super()

		this.radius = radius

		this.on("added", this.onAdded, this)
		this.on("removed", this.onRemoved, this)
	}

	onAdded()
	{
		Main.instance.app.ticker.add(this.update, this)
	}

	onRemoved()
	{
		Main.instance.app.ticker.remove(this.update, this)
	}

	update()
	{
		this.speedY += this.gravity

		this.x += this.speedX
		this.y += this.speedY
	}

	didHitGround(angle: number)
	{
		this.x -= Math.cos(angle)*0.3
		this.y -= Math.sin(angle)*0.3
		this.speedX *= 1 - Math.abs(Math.cos(angle))
		this.speedY *= 1 - Math.abs(Math.sin(angle))
		this.speedX *= 0.9
		this.speedY *= 0.95
	}

	didHitGameObject(gameObject: GameObject)
	{
		
	}
}