import Stage from "./Stage";
import newGameScreenImageURL from './res/new_game.png'
import Main from "./main";
import GameStage from "./GameStage";

export default class MenuStage extends Stage
{
	constructor()
	{
		super()

		var background = new PIXI.Sprite(PIXI.Texture.fromImage(newGameScreenImageURL))
		this.addChild(background)

		background.interactive = true
		background.on("mouseup", (event: PIXI.interaction.InteractionEvent) =>
		{
			Main.instance.showStage(new GameStage())
		});
	}
}