import enemyImageURL from './res/monster_00.png'

import Character from "./Character";
import ShotgunWeapon from './GameObjects/ShotgunWeapon';
import GameStage from './GameStage';

export default class Enemy extends Character
{
	targetSpeed: number

	constructor()
	{
		super(40, new ShotgunWeapon())

		var sprite = new PIXI.Sprite(PIXI.Texture.from(enemyImageURL))
		sprite.anchor.x = 0.5
		sprite.anchor.y = 0.5
		this.addChild(sprite)

		this.targetSpeed = 3
	}

	update()
	{
		super.update()

		this.ai()
	}

	ai()
	{
		if (this.canFireTarget(GameStage.instance.player.x, GameStage.instance.player.y)) {
			this.fire(GameStage.instance.player.x, GameStage.instance.player.y)
		}

		if (this.isHittingGroundOnTheRight() && this.targetSpeed == 3) {
			this.targetSpeed = -3
		}
		if (this.isHittingGroundOnTheLeft() && this.targetSpeed == -3) {
			this.targetSpeed = 3
		}

		this.speedX = this.targetSpeed
	}

	canFireTarget(targetX: number, targetY: number): boolean
	{
		var testX: number = this.x
		var testY: number = this.y
		let dx = targetX - this.x
		let dy = targetY - this.y
		var distance = Math.hypot(dx, dy)
		var step = 20
		let stepX = dx/distance * step
		let stepY = dy/distance * step

		while (true) {
			testX += stepX
			testY += stepY

			if (GameStage.instance.currentLevel.isHit(testX, testY)) {
				return false
			}

			var distanceToTarget = Math.hypot(targetX - testX, targetY - testY)
			if (distanceToTarget < 30) {
				return true
			}
		}

		return true
	}

	isHittingGroundOnTheLeft()
	{
		return GameStage.instance.currentLevel.isHit(this.x - this.radius*1.2, this.y)
	}

	isHittingGroundOnTheRight()
	{
		return GameStage.instance.currentLevel.isHit(this.x + this.radius*1.2, this.y)
	}
}