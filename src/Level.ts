import Character from "./Character";
import Main from "./main";
import GameObject from "./GameObject";

export default class Level extends PIXI.Sprite
{
	foreground: PIXI.Sprite
	pixelBytes: any

	constructor(foregroundImageUrl: string)
	{
		super()

		var texture = PIXI.Texture.from(foregroundImageUrl)
		this.foreground = new PIXI.Sprite(texture)
		
		texture.addListener("update", (texture) => {
			this.pixelBytes = Main.instance.app.renderer.extract.pixels(this.foreground)
		})

		this.addChild(this.foreground)
	}

	collide(gameObject: GameObject)
	{
		for (var i=0; i<5; i++) {
			for (var angle = 0; angle < Math.PI*2; angle += Math.PI/4) {
				var x = gameObject.x + Math.cos(angle) * gameObject.radius
				var y = gameObject.y + Math.sin(angle) * gameObject.radius
				if (this.isHit(x, y)) {
					gameObject.didHitGround(angle)
				}
			}
		}
	}

	isHit(x: number, y: number): boolean
	{
		var pixelPosition = Math.round(y)*this.foreground.width + Math.round(x)
		if (this.pixelBytes) {
			return this.pixelBytes[pixelPosition*4+3] == 255
		} else {
			return false
		}
	}
}