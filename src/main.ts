import * as PIXI from 'pixi.js'
import Stage from './Stage';
import MenuStage from './MenuStage';

export default class Main
{
	app = new PIXI.Application(1000, 600)

	static instance: Main

	constructor()
	{
		Main.instance = this
		document.body.appendChild(this.app.view)

		var menu = new MenuStage()
		this.showStage(menu)
	}

	showStage(stage: Stage)
	{
		this.app.stage.removeChildren()
		this.app.stage.addChild(stage)
	}
}

let main = new Main()