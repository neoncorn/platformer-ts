import Main from "./main";
import GameStage from "./GameStage";
import Bullet from "./Bullet";
import GameObject from "./GameObject";
import Weapon from "./Weapon";
import Explosion from "./Explosion";

export default class Character extends GameObject
{
	radius: number
	lastFireTime: number = 0
	weapon: Weapon
	health: number = 100

	constructor(radius: number, weapon: Weapon)
	{
		super(radius)

		this.radius = radius
		this.weapon = weapon
	}

	jump()
	{
		if (GameStage.instance.currentLevel.isHit(this.x - this.radius*0.8, this.y + this.radius*1.3)
		 || GameStage.instance.currentLevel.isHit(this.x + this.radius*0.8, this.y + this.radius*1.3)) {
			this.speedY = -10
		}
	}

	fire(targetX: number, targetY: number)
	{
		var now = (new Date()).getTime()
		var timeSinceFire = now - this.lastFireTime

		if (timeSinceFire > this.weapon.fireRate) {
			let dx = targetX - this.x
			let dy = targetY - this.y
			var length = Math.sqrt(dx*dx + dy*dy)
			let speedX = dx/length * this.weapon.speed
			let speedY = dy/length * this.weapon.speed

			new Bullet(this, this.weapon.damage, this.weapon.bulletImageUrl, speedX, speedY)
			this.lastFireTime = now
		}
	}

	didHitGameObject(gameObject: GameObject)
	{
		if (gameObject instanceof Bullet) {
			if (gameObject.owner != this) {
				this.health = this.health - gameObject.damage
			}
		}
	}

	update()
	{
		super.update()

		if (this.health <= 0) {
			GameStage.instance.removeGameObject(this)
			this.explode()
		}
	}

	explode()
	{
		for (var i=0; i<50; i++) {
			new Explosion(
				this.x + Math.random()*this.radius*2 - this.radius,
				this.y + Math.random()*this.radius*2 - this.radius
			)
		}
	}
}