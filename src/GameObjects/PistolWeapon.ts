import Weapon from "../Weapon";
import bulletImageUrl from '../res/bullet_pistol.png'

export default class PistolWeapon extends Weapon
{
	damage: number = 30
	speed: number = 5
	fireRate: number = 200
	bulletImageUrl: string = bulletImageUrl
	itemImageUrl: string = ""
	playerSpeedModification: number = 0
	scatter: number = 0
}