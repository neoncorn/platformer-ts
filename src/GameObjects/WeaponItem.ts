import GameObject from "../GameObject";
import Weapon from "../Weapon";
import GameStage from "../GameStage";

export default class WeaponItem extends GameObject
{
	weapon: Weapon

	constructor(weapon: Weapon)
	{
		super(22)

		this.weapon = weapon

		var sprite = new PIXI.Sprite(PIXI.Texture.from(weapon.itemImageUrl))
		sprite.anchor.x = 0.5
		sprite.anchor.y = 0.5
		this.addChild(sprite)
	}

	didHitGround(angle: number)
	{
		this.x -= Math.cos(angle)*0.3
		this.y -= Math.sin(angle)*0.3
		this.speedX = -this.speedX
        this.speedY = -this.speedY
    }
    
    didHitGameObject(gameObject: GameObject)
    {
      if (gameObject == GameStage.instance.player) {
          GameStage.instance.player.weapon = this.weapon
          GameStage.instance.removeGameObject(this)
      }
      
    }
}