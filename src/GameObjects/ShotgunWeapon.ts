import Weapon from "../Weapon";
import bulletImageUrl from '../res/bullet_rocket.png'
import shotgunSprite from '../res/shotgunSprite.png'

export default class ShotgunWeapon extends Weapon
{
	damage: number = 30
	speed: number = 3
	fireRate: number = 2000
	bulletImageUrl: string = bulletImageUrl
	itemImageUrl: string = shotgunSprite
	playerSpeedModification: number = 0
	scatter: number = 0.3
}